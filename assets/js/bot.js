var me = {};
me.avatar = "https://i0.wp.com/marinabarletta.com.br/wp-content/uploads/2015/05/img-medica-perfil.jpg?ssl=1";

var you = {};
you.avatar = "https://a11.t26.net/taringa/avatares/9/1/2/F/7/8/Demon_King1/48x48_5C5.jpg";

var response;

//-- No use time. It is a javaScript effect.
function insertChat(who, text, time) {
    if (time === undefined) {
        time = 0;
    }
    var control = "";

    if (who == "me") {
        control = '<div class="message bot">' + text + '</div > ';
        carregando = '<div class="message bot loading-bot">' +
            '  <div class="typing typing-1"></div>' +
            '  <div class="typing typing-2"></div>' +
            '  <div class="typing typing-3"></div>' +
            '</div>';
        //Simulando o digitar e a leitura do bot
        $(".input").hide();
        setTimeout(function () {
            $(".messages").append(carregando).scrollTop($(".messages").prop('scrollHeight'));
        }, 300);
        setTimeout(function () {
            $(".input").show();
            $(".messages").append(control).scrollTop($(".messages").prop('scrollHeight'));
            $(".loading-bot").remove();
        }, 2000);
    } else {
        control = '<div class="message parker">' +
            text +
            '</div>';
        $(".messages").append(control).scrollTop($(".messages").prop('scrollHeight'));
    }

}

function resetChat() {
    $('.resetmytext').hide();
    $(".messages").empty();
}

//-- Buttons
$(".mytext").on("keydown", function (e) {
    if (e.which == 13) {
        var text = $(this).val();
        if (text !== "") {
            insertChat("me", text);
            $(this).val('');
        }
    }
});

$('body > div > div > div:nth-child(2) > span').click(function () {
    $(".numbersmytext").trigger({ type: 'keydown', which: 13, keyCode: 13 });
});

//Respondendo
function getAnswer(index) {
    index++;

    if (vocabulary[index] !== undefined) {
        insertChat("me", vocabulary[index]);
    } else {
        $('.mytext').hide();
        $('.numbersmytext').hide();
        $('.resetmytext').show();
        final_answer = getFinalAnswer();
        insertChat("me", final_answer);
    }
    return index;
}
//Respondendo o veredito final
function getFinalAnswer() {
    var grupo1 = 0; var grupo2 = 0;
    for (let i = 1; i < results.length; i++) {
        const element = results[i];
        if (jQuery.inArray(i, [6, 7, 9, 11]) !== -1) {

        } else {
            if (i <= 4) {
                grupo1 = grupo1 + parseInt(element);
            }
            if (i >= 4) {
                grupo2 = grupo2 + parseInt(element);
            }
        }
    }

    if (grupo1 >= 2) {
        if (grupo2 > 1) {
            return answer[0];
        } else {
            return answer[1];
        }
    } else {
        return answer[1];
    }
}

//Alterando para inserir texto
function changeToText() {
    $('.mytext').hide();
    $('.numbersmytext').show();
}
//Alterando para botões 'sim' e 'não'
function changeToButton() {
    $('.numbersmytext').hide();
    $('.mytext').show();
}

$(".resetmytext").click(function () {
    startBot();
});

function startBot() {
    //-- Clear Chat
    resetChat();
    vocabulary = [
        'Olá, eu sou o Chat Bot do Hospital Sabará, irei lhe fazer algumas perguntas',//0
        'Seu filho tem febre?',//1
        'Seu filho tem TOSSE ou outro sintoma respiratório (CORIZA ou DOR DE GARGANTA)',//2
        'Seu filho sente dificuldade para respirar?',//3
        'Seu filho tem pelo menos um dos seguintes sintomas: DOR DE CABEÇA, DOR NO CORPO ou INDISPOSIÇÃO?',//4
        'Seu filho esteve na China nos últimos 21 dias?',//5
        'Há quantos dias você saiu da China?',//6
        'Há quantos dias os sintomas tiveram início?',//7
        'Seu filho teve contato com alguém que retornou da China nos últimos 21 dias?',//8
        'Há quantos dias você teve contato com essa pessoa?',//9
        'Você teve contato com caso suspeito ou confirmado de coronavírus nos últimos 21 dias?',//10
        'Há quantos dias você teve contato com o caso suspeito:'//11
    ];

    results = [];

    answer = [
        '“De acordo com as definições do Ministério da Saúde, existe possibilidade dos sintomas serem relacionados ao novo coronavírus. Recomendamos que procure o seu pediatra de confiança ou vá a um serviço médico mais próximo de sua residência”<br>' +
        '<br>Caso deseje ser atendido no Pronto-Socorro do Hospital Infantil Sabará, solicitamos que ligue para nossa “Hot line” (11) 3155-2800 para receber orientação. </p>' +
        '<br>Outras recomendações:<br> Todas as pessoas sintomáticas devem usar máscara cobrindo nariz e boca ao sair de casa. Higienize as mãos com água e sabão ou álcool gel com frequência.<br>',
        'De acordo com as definições do Ministério da Saúde, é pouco provável que os sintomas sejam relacionados ao novo coronavírus. Recomendamos que procure o seu pediatra de confiança ou vá a um serviço médico mais próximo de sua residência para receber orientação adequada.'
    ];

    //-- Print Messages
    insertChat("me", vocabulary[0]);
    insertChat("me", vocabulary[1]);
    var index = 1;

    changeToButton();

    $('.numbersmytext').keypress(function (event) {
        if (!$(".loading-bot").length && $("#chat").children().last().attr('class') !== 'message parker') {
            if (index < vocabulary.length) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13' && this.value !== '') {
                    response = this.value;
                    insertChat("you", response);
                    results[index] = response;
                    index = getAnswer(index);
                    $(this).val('');
                }
                if (jQuery.inArray(index, [6, 7, 9, 11]) === -1) {
                    changeToButton();
                }
            }
        }
    });

    $(".mytext").click(function () {
        
        if (!$(".loading-bot").length && $("#chat").children().last().attr('class') !== 'message parker') {
            if (index < vocabulary.length) {
                if (this.value === "sim") {
                    insertChat("you", "Sim");
                    response = 1;
                    results[index] = response;
                } else {
                    insertChat("you", "Não");
                    response = 0;
                    results[index] = response;
                }

                if (response === 0) {
                    switch (index) {
                        case 5:
                            index = 7;
                            break;
                        case 8:
                            index = 9;
                            break;
                        case 10:
                            index = 11;
                            break;
                        default:
                            break;
                    }
                }

                index = getAnswer(index);
                if (jQuery.inArray(index, [6, 7, 9, 11]) !== -1) {
                    changeToText();
                }
            }
        }
    });


}

$(document).ready(function () {
    startBot();
    if ($('img[alt="www.000webhost.com"]')) {
        $('img[alt="www.000webhost.com"]').remove();
    }
});



